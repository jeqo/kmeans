package com.jeqo.kmeans.kmeans;

import java.util.*;

public class Cluster {

    private final List<Vector> points;
    private Vector centroid;

    public Cluster(Vector firstPoint) {
        points = new ArrayList<Vector>();
        centroid = firstPoint;
    }

    public Vector getCentroid() {
        return centroid;
    }

    public void updateCentroid() {
        double newx[] = new double[Vector.len];
        for (int k = 0; k < Vector.len; k++) {
            for (Vector point : points) {
                newx[k] = newx[k] + point.x[k];
            }
            newx[k] = newx[k] / points.size();
        }
        centroid = new Vector(newx, "");
    }

    public List<Vector> getPoints() {
        return points;
    }

    public String toString() {
        StringBuilder builder = new StringBuilder("This cluster contains the following points:\n");
        for (Vector point : points) {
            builder.append(point.toString() + ",\n");
        }
        return builder.deleteCharAt(builder.length() - 2).toString();
    }
}
