package com.jeqo.kmeans.kmeans;

public class Vector {

    static int len = 2;
    static boolean hasId = true;
    private int index = -1;	//denotes which Cluster it belongs to
    public double x[];
    String id;

    public Vector(double[] xx, String theId) {
        this.x = new double[len];
        this.x = xx;
        id = theId;
    }

    // change the distance measure 
    public Double getSquareOfDistance(Vector anotherV) {
        double squareOfdist = 0.0;
        for (int k = 0; k < len; k++) {
            squareOfdist += (x[k] - anotherV.x[k]) * (x[k] - anotherV.x[k]);
        }
        return squareOfdist;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String toString() {
        String str = id + "(";
        int k;
        for (k = 0; k < len - 1; k++) {
            str += x[k] + ",";
        }
        str += x[k] + ")";
        return str;
    }
}
